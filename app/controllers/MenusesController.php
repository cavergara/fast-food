<?php

class MenusesController extends \BaseController {

	/**
	 * Display a listing of menuses
	 *
	 * @return Response
	 */
	public function index()
	{
		$menuses = Menus::all();

		return View::make('menuses.index', compact('menuses'));
	}

	/**
	 * Show the form for creating a new menus
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('menuses.create');
	}

	/**
	 * Store a newly created menus in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Menus::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Menus::create($data);

		return Redirect::route('menuses.index');
	}

	/**
	 * Display the specified menus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$menus = Menus::findOrFail($id);

		return View::make('menuses.show', compact('menus'));
	}

	/**
	 * Show the form for editing the specified menus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$menus = Menus::find($id);

		return View::make('menuses.edit', compact('menus'));
	}

	/**
	 * Update the specified menus in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$menus = Menus::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Menus::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$menus->update($data);

		return Redirect::route('menuses.index');
	}

	/**
	 * Remove the specified menus from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Menus::destroy($id);

		return Redirect::route('menuses.index');
	}

}
