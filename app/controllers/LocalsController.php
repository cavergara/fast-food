<?php

class LocalsController extends \BaseController {

	/**
	 * Display a listing of locals
	 *
	 * @return Response
	 */
	public function index()
	{
		$locals = Local::all();

		return View::make('locals.index', compact('locals'));
	}

	/**
	 * Show the form for creating a new local
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('locals.create');
	}

	/**
	 * Store a newly created local in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Local::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Local::create($data);

		return Redirect::route('locals.index');
	}

	/**
	 * Display the specified local.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$local = Local::findOrFail($id);

		return View::make('locals.show', compact('local'));
	}

	/**
	 * Show the form for editing the specified local.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$local = Local::find($id);

		return View::make('locals.edit', compact('local'));
	}

	/**
	 * Update the specified local in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$local = Local::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Local::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$local->update($data);

		return Redirect::route('locals.index');
	}

	/**
	 * Remove the specified local from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Local::destroy($id);

		return Redirect::route('locals.index');
	}

}
