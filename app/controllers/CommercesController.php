<?php

class CommercesController extends \BaseController {

	/**
	 * Display a listing of commerces
	 *
	 * @return Response
	 */
	public function index()
	{
		$commerces = Commerce::all();

		return View::make('commerces.index', compact('commerces'));
	}

	/**
	 * Show the form for creating a new commerce
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('commerces.create');
	}

	/**
	 * Store a newly created commerce in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Commerce::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Commerce::create($data);

		return Redirect::route('commerces.index');
	}

	/**
	 * Display the specified commerce.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$commerce = Commerce::findOrFail($id);

		return View::make('commerces.show', compact('commerce'));
	}

	/**
	 * Show the form for editing the specified commerce.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$commerce = Commerce::find($id);

		return View::make('commerces.edit', compact('commerce'));
	}

	/**
	 * Update the specified commerce in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$commerce = Commerce::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Commerce::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$commerce->update($data);

		return Redirect::route('commerces.index');
	}

	/**
	 * Remove the specified commerce from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Commerce::destroy($id);

		return Redirect::route('commerces.index');
	}

}
