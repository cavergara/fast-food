<?php

class ItemsOrdersController extends \BaseController {

	/**
	 * Display a listing of itemsorders
	 *
	 * @return Response
	 */
	public function index()
	{
		$itemsorders = Itemsorder::all();

		return View::make('itemsorders.index', compact('itemsorders'));
	}

	/**
	 * Show the form for creating a new itemsorder
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('itemsorders.create');
	}

	/**
	 * Store a newly created itemsorder in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Itemsorder::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Itemsorder::create($data);

		return Redirect::route('itemsorders.index');
	}

	/**
	 * Display the specified itemsorder.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$itemsorder = Itemsorder::findOrFail($id);

		return View::make('itemsorders.show', compact('itemsorder'));
	}

	/**
	 * Show the form for editing the specified itemsorder.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$itemsorder = Itemsorder::find($id);

		return View::make('itemsorders.edit', compact('itemsorder'));
	}

	/**
	 * Update the specified itemsorder in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$itemsorder = Itemsorder::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Itemsorder::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$itemsorder->update($data);

		return Redirect::route('itemsorders.index');
	}

	/**
	 * Remove the specified itemsorder from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Itemsorder::destroy($id);

		return Redirect::route('itemsorders.index');
	}

}
